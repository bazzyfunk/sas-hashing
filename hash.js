// Initialisation de la chaîne vide
let str = "";

// Fonction de hashing
const calculateHash = str => {
  let hash = str
    .split("")
    .map((c, i) => str.charCodeAt(i))
    .map(c => c + 2)
    .map(c => String.fromCharCode(c))
    .join("");
  return Buffer.from(hash).toString("base64");
};

// Connaître le hash d'un mot de 4 lettres
console.log(calculateHash("test"));

const alphabet = "abcdefghijklmnopqrstuvwxyz";

// Séparer l'alphabet en 26 charactères
const splitAlphabet = alphabet.match(/.{1,1}/g);

// Initialisation des index pour chaque lettre du mot à trouver
let i = 0;
let j = 0;
let k = 0;
let l = 0;

// Bruteforcing du hash de "test"
// Si le console.log retourne "test" alors,
// La boucle fonctionne avec tous les hash de 4 mots
// Y compris avec ZWpxZQ== (choc) ou eWd1ag== (wesh)
while (calculateHash(str) !== "dmd1dg==") {
  str =
    splitAlphabet[i] + splitAlphabet[j] + splitAlphabet[k] + splitAlphabet[l];

  l += 1;

  // De 'aaaz' à 'zzzz'
  if (l === 26) {
    k += 1;
    l = 0;
  }

  // De 'aazz' à 'zzzz'
  if (k === 26) {
    j += 1;
    k = 0;
  }

  // De 'azzz' à 'zzzz'
  if (j === 26) {
    i += 1;
    j = 0;
  }

  if (i === 26) {
    break;
  }
}

// Affichage du mot à chercher
console.log(str);
